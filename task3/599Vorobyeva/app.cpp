#include <common/Application.hpp>
#include <common/LightInfo.hpp>
#include <common/Mesh.hpp>
#include <common/ShaderProgram.hpp>
#include <common/Texture.hpp>
#include <common/Camera.hpp>

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <iostream>
#include <sstream>
#include <vector>


namespace
{
    float frand()
    {
        return static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
    }
}

class SampleApplication : public Application {
public:
	MeshPtr _tree;
	MeshPtr _ground;
	MeshPtr _cube;
	MeshPtr _marker; 

	DataBufferPtr _bufVec3;
	DataBufferPtr _bufVec4;

	ShaderProgramPtr _commonShader;
	ShaderProgramPtr _markerShader;
	ShaderProgramPtr _cullShader;
	ShaderProgramPtr _shaderTess;

	//Переменные для управления положением одного источника света
	float _lr = 3.0;
	float _phi = 0.0;
	float _theta = glm::pi<float>() * 0.25f;

	LightInfo _light;

	TexturePtr _treeTexture;
	TexturePtr _groundTexture;
	TexturePtr _cubeTexture;
	TexturePtr _bufferTexCulled;

	GLuint _groundSampler;
	GLuint _treeSampler;
	GLuint _cubeSampler;

	GLuint _TF; //Объект для хранения настроект Transform Feedback
    GLuint _cullVao; //VAO, который хранит настройки буфера для чтения данных во время Transform Feedback
    GLuint _tfOutputVbo; //VBO, в который будут записываться смещения моделей после отсечения

	GLuint _query; //Переменная-счетчик, куда будет записываться количество пройденных отбор моделей


	std::vector<glm::vec3> _positionsVec3;
	std::vector<glm::vec3> _positionsCubesVec3;
	std::vector<glm::vec4> _positionsVec4;
	const unsigned int K = 300; //Количество инстансов
	const unsigned int _cubes_cnt = 20; // Количество кубов
	float _cubeSize = 4.0f;

	SampleApplication()
	{
		_cameraMover = std::make_shared<FreeCameraMover>();
		_cameraMover->setSpeed( 4.0 );
	}

	void makeScene() override
	{
		Application::makeScene();

		//=========================================================
		//Создание и загрузка мешей		

		_ground = makeGroundPlane( -100, 10 );
		_ground->setModelMatrix( glm::translate( glm::mat4( 1.0f ), glm::vec3( 0.0f, 0.0f, -0.0001f ) ) );

		//=========================================================
		// Позиции кубов

		srand((int) glfwGetTime() * 1000);
		const float cubeSize = 200.0f;
        for (unsigned int i = 0; i < _cubes_cnt; i++)
        {
            _positionsCubesVec3.push_back(glm::vec3(frand() * cubeSize - 0.5 * cubeSize,
            	frand() * cubeSize - 0.5 * cubeSize, 0.0));
        }

		//=========================================================
        //Инициализируем K случайных сдвигов для K экземпляров
        //Некоторые варианты требуют выровненный массив по vec4, а некоторые нет

        srand((int)(glfwGetTime() * 1000));

        const float size = 100.0f;
        for (unsigned int i = 0; i < K; i++)
        {
            _positionsVec3.push_back(glm::vec3(frand() * size - 0.5 * size, frand() * size - 0.5 * size, 0.0));
            _positionsVec4.push_back(glm::vec4(_positionsVec3.back(), 0.0));
        }

        //=========================================================

        //Создаем буферы без выравнивания (_bufVec3) и с выравниванием (_bufVec4)

        _bufVec3 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        _bufVec3->setData(_positionsVec3.size() * sizeof(float) * 3, _positionsVec3.data());

        _bufVec4 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        _bufVec4->setData(_positionsVec4.size() * sizeof(float) * 4, _positionsVec4.data());

        //----------------------------

        //Привязываем SSBO к 0й точке привязки (требуется буфер с выравниванием)
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, _bufVec4->id());

        //----------------------------

		//===========================================================
		// Tree mesh
		const std::string modelName = "599VorobyevaData3/models/tree2.obj";
        _tree = loadFromFile(modelName);
        _tree->setModelMatrix( glm::translate( glm::mat4( 1.0f ), 
				glm::vec3( 0.0f, 0.0f, 0.0f ) ) );
		//===========================================================
        // Cube mesh

        _cube = makeCube(_cubeSize);
        _cube->setModelMatrix(glm::translate( glm::mat4( 1.0f ),
				glm::vec3( 0.0f, 0.0f, 4.0f ) ));

		std::cout << "Meshes done" << std::endl;

		_marker = makeSphere( 0.1f );

		//=========================================================
		//Инициализация шейдеров

		_commonShader = std::make_shared<ShaderProgram>("599VorobyevaData3/shaders/instancingSSBO.vert",
			"599VorobyevaData3/shaders/common.frag");

		_markerShader = std::make_shared<ShaderProgram>( "599VorobyevaData3/shaders/marker.vert", "599VorobyevaData3/shaders/marker.frag" );

		std::cout << "Shaders initialized" << std::endl;
		//=========================================================
		// GPU Culling
		//=========================================================
		_cullShader = std::make_shared<ShaderProgram>();

		ShaderPtr vs_cull = std::make_shared<Shader>(GL_VERTEX_SHADER);
        vs_cull->createFromFile("599VorobyevaData3/shaders/cull.vert");
        _cullShader->attachShader(vs_cull);

		ShaderPtr gs = std::make_shared<Shader>(GL_GEOMETRY_SHADER);
        gs->createFromFile("599VorobyevaData3/shaders/cull.geom");
        _cullShader->attachShader(gs);

        //Выходные переменные, которые будут записаны в буфер
        const char* attribs[] = { "position" };
        std::cout << "Geom shaders done" << std::endl;
        glTransformFeedbackVaryings(_cullShader->id(), 1, attribs, GL_SEPARATE_ATTRIBS);

        _cullShader->linkProgram();

        std::cout << "Cull shaders done" << std::endl;
        //----------------------------

        //VAO, который будет поставлять данные для отсечения
        glGenVertexArrays(1, &_cullVao);
        glBindVertexArray(_cullVao);

        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, _bufVec3->id());
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

        glBindVertexArray(0);

        std::cout << "VAO done" << std::endl;
        //----------------------------
        //Инициализируем буфер, в который будут скопированы смещения моделей после отсечения
        glGenBuffers(1, &_tfOutputVbo);
        glBindBuffer(GL_ARRAY_BUFFER, _tfOutputVbo);
        glBufferData(GL_ARRAY_BUFFER, _positionsVec3.size() * sizeof(float) * 3, 0, GL_STREAM_DRAW);

        glBindBuffer(GL_ARRAY_BUFFER, 0);

        //Настроечный объект Transform Feedback
        glGenTransformFeedbacks(1, &_TF);
        glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, _TF);
        glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, _tfOutputVbo);
        glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, 0);

        glGenQueries(1, &_query);
		//========================================================
		//Инициализация значений переменных освещения
		_light.position = glm::vec3( glm::cos( _phi ) * glm::cos( _theta ), glm::sin( _phi ) * glm::cos( _theta ), glm::sin( _theta ) ) * _lr;
		_light.ambient = glm::vec3( 0.2, 0.2, 0.2 );
		_light.diffuse = glm::vec3( 0.8, 0.8, 0.8 );
		_light.specular = glm::vec3( 1.0, 1.0, 1.0 );

		std::cout << "Lights initialized" << std::endl;
		//=========================================================
		//Загрузка и создание текстур
		_treeTexture = loadTexture( "599VorobyevaData3/images/grass.jpg" );
		_groundTexture = loadTexture( "599VorobyevaData3/images/grass.jpg" );
		_cubeTexture = loadTexture("599VorobyevaData3/images/brick_spec.jpg");

		//Создаем текстурный буфер и привязываем к нему буфер без выравнивания
        _bufferTexCulled = std::make_shared<Texture>(GL_TEXTURE_BUFFER);
        _bufferTexCulled->bind();
        glTexBuffer(GL_TEXTURE_BUFFER, GL_RGB32F_ARB, _tfOutputVbo);
        _bufferTexCulled->unbind();

		std::cout << "Textures initialized" << std::endl;
		//=========================================================
		//Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
		glGenSamplers( 1, &_groundSampler );
		glSamplerParameteri( _groundSampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
		glSamplerParameteri( _groundSampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
		glSamplerParameteri( _groundSampler, GL_TEXTURE_WRAP_S, GL_REPEAT );
		glSamplerParameteri( _groundSampler, GL_TEXTURE_WRAP_T, GL_REPEAT );

		glGenSamplers( 1, &_treeSampler );
		glSamplerParameteri( _treeSampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
		glSamplerParameteri( _treeSampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
		glSamplerParameteri( _treeSampler, GL_TEXTURE_WRAP_S, GL_REPEAT );
		glSamplerParameteri( _treeSampler, GL_TEXTURE_WRAP_T, GL_REPEAT );
		glSamplerParameteri( _treeSampler, GL_TEXTURE_WRAP_R, GL_REPEAT );

		glGenSamplers( 1, &_cubeSampler );
		glSamplerParameteri( _cubeSampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
		glSamplerParameteri( _cubeSampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
		glSamplerParameteri( _cubeSampler, GL_TEXTURE_WRAP_S, GL_REPEAT );
		glSamplerParameteri( _cubeSampler, GL_TEXTURE_WRAP_T, GL_REPEAT );
		glSamplerParameteri( _cubeSampler, GL_TEXTURE_WRAP_R, GL_REPEAT );


		std::cout << "Make scene finished" << std::endl;

	}

	void cull(const ShaderProgramPtr& shader)
    {
        shader->use();

        shader->setFloatUniform("time", static_cast<float>(glfwGetTime()));

        glEnable(GL_RASTERIZER_DISCARD);

        glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, _TF);

        glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, _query);

        glBeginTransformFeedback(GL_POINTS);

        glBindVertexArray(_cullVao);
        glDrawArrays(GL_POINTS, 0, _positionsVec3.size());

        glEndTransformFeedback();

        glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);

        glDisable(GL_RASTERIZER_DISCARD);
    }

	void draw() override
	{
		//Получаем текущие размеры экрана и выставлям вьюпорт
		int width, height;
		glfwGetFramebufferSize( _window, &width, &height );
		cull(_cullShader);
		GLuint primitivesWritten;
        glGetQueryObjectuiv(_query, GL_QUERY_RESULT, &primitivesWritten);
		glViewport( 0, 0, width, height );

		//Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
		glClearColor( 1.0f, 1.0f, 1.0f, 1.0f );
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

		_cameraMover->setSpeed(4.0);
		drawScene(_commonShader, primitivesWritten);

		//Отсоединяем сэмплер и шейдерную программу
		glBindSampler( 0, 0 );
		glUseProgram( 0 );
	}

	void drawScene(const ShaderProgramPtr& shader, const GLuint &primitivesWritten)
    {
        shader->use();

		//Загружаем на видеокарту значения юниформ-переменных
		shader->setMat4Uniform( "viewMatrix", _camera.viewMatrix );
		shader->setMat4Uniform( "projectionMatrix", _camera.projMatrix );

		_light.position = glm::vec3( glm::cos( _phi ) * glm::cos( _theta ), glm::sin( _phi ) * glm::cos( _theta ), glm::sin( _theta ) ) * _lr * 10.f;
		glm::vec3 lightPosCamSpace = glm::vec3( _camera.viewMatrix * glm::vec4( _light.position, 1.0 ) );
		shader->setVec3Uniform( "light.pos", lightPosCamSpace ); //копируем положение уже в системе виртуальной камеры
		shader->setVec3Uniform( "light.La", _light.ambient );
		shader->setVec3Uniform( "light.Ld", _light.diffuse );
		shader->setVec3Uniform( "light.Ls", _light.specular );

		// GROUND ===============================================================
		glActiveTexture( GL_TEXTURE0 );  //текстурный юнит 0
		glBindSampler( 0, _groundSampler );
		_groundTexture->bind();
		shader->setIntUniform( "diffuseTex", 0 );
		shader->setMat4Uniform( "modelMatrix", _ground->modelMatrix() );
		shader->setMat3Uniform( "normalToCameraMatrix", glm::transpose( glm::inverse( glm::mat3( _camera.viewMatrix * _ground->modelMatrix() ) ) ) );
		_ground->draw();
		
		// TREES ================================================================

		glActiveTexture( GL_TEXTURE0 );  //текстурный юнит 0
		glBindSampler( 0, _treeSampler );
		_treeTexture->bind();
		shader->setIntUniform( "treeTex", 0 );
		glActiveTexture(GL_TEXTURE1);
		_bufferTexCulled->bind();
		shader->setIntUniform("texBuf", 1);
		shader->setMat4Uniform( "modelMatrix", _tree->modelMatrix());
		shader->setMat3Uniform( "normalToCameraMatrix", glm::transpose( glm::inverse( glm::mat3( _camera.viewMatrix * _tree->modelMatrix() ) ) ) );
		unsigned int ssboIndex = glGetProgramResourceIndex(shader->id(), GL_SHADER_STORAGE_BLOCK, "Positions");
        glShaderStorageBlockBinding(shader->id(), ssboIndex, 0); //0я точка привязки
        _tree->drawInstanced(primitivesWritten);
        _tree->draw();
		
		// CUBES ==================================================================

		glActiveTexture( GL_TEXTURE0 );  //текстурный юнит 0
		glBindSampler( 0, _cubeSampler );
		_cubeTexture->bind();
		shader->setIntUniform( "cubeTex", 0 );

		for(int i = 0; i < _positionsCubesVec3.size(); ++i) {
			shader->setMat4Uniform( "modelMatrix", glm::translate(_cube->modelMatrix(), _positionsCubesVec3[i]) );
			shader->setMat3Uniform( "normalToCameraMatrix",
				glm::transpose( glm::inverse( glm::mat3( _camera.viewMatrix * _cube->modelMatrix() ) ) ) );

			_cube->draw();
		}

		// LIGHT ===================================================================		
		_markerShader->use();

		_markerShader->setMat4Uniform( "mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate( glm::mat4( 1.0f ), _light.position ) );
		_markerShader->setVec4Uniform( "color", glm::vec4( _light.diffuse, 1.0f ) );
		_marker->draw();

		//Отсоединяем сэмплер и шейдерную программу
		glBindSampler( 0, 0 );
		glUseProgram( 0 );

        
    }
};

int main()
{
	SampleApplication app;
	app.start();

	return 0;
}
