# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/litleo/Documents/katikGL/pull_request/master/task3/599Vorobyeva/gl1_SimpleTriangle.cpp" "/home/litleo/Documents/katikGL/pull_request/master/task3/599Vorobyeva/CMakeFiles/123Ivanov2.dir/gl1_SimpleTriangle.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GLEW_STATIC"
  "GLFW_DLL"
  "GLM_FORCE_DEPTH_ZERO_TO_ONE"
  "GLM_FORCE_PURE"
  "GLM_FORCE_RADIANS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "external/glew-1.13.0/include"
  "external/GLM"
  "external/SOIL/src/SOIL2"
  "external/Assimp/include"
  "external/GLFW/include"
  "external/imgui"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/litleo/Documents/katikGL/pull_request/master/external/glew-1.13.0/build/cmake/CMakeFiles/glew_s.dir/DependInfo.cmake"
  "/home/litleo/Documents/katikGL/pull_request/master/external/SOIL/CMakeFiles/soil.dir/DependInfo.cmake"
  "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/CMakeFiles/assimp.dir/DependInfo.cmake"
  "/home/litleo/Documents/katikGL/pull_request/master/external/imgui/CMakeFiles/imgui.dir/DependInfo.cmake"
  "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/contrib/irrXML/CMakeFiles/IrrXML.dir/DependInfo.cmake"
  "/home/litleo/Documents/katikGL/pull_request/master/external/GLFW/src/CMakeFiles/glfw.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
