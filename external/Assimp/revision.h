#ifndef ASSIMP_REVISION_H_INC
#define ASSIMP_REVISION_H_INC

#define GitVersion 0xe7735af
#define GitBranch "master"

#endif // ASSIMP_REVISION_H_INC
