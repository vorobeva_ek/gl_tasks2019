# Install script for directory: /home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libassimp.so.4.1.0"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libassimp.so.4"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libassimp.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_CHECK
           FILE "${file}"
           RPATH "")
    endif()
  endforeach()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY FILES
    "/home/litleo/Documents/katikGL/pull_request/master/lib/libassimp.so.4.1.0"
    "/home/litleo/Documents/katikGL/pull_request/master/lib/libassimp.so.4"
    "/home/litleo/Documents/katikGL/pull_request/master/lib/libassimp.so"
    )
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libassimp.so.4.1.0"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libassimp.so.4"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libassimp.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      if(CMAKE_INSTALL_DO_STRIP)
        execute_process(COMMAND "/usr/bin/strip" "${file}")
      endif()
    endif()
  endforeach()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xassimp-devx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/assimp" TYPE FILE FILES
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/anim.h"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/ai_assert.h"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/camera.h"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/color4.h"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/color4.inl"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/config.h"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/defs.h"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/Defines.h"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/cfileio.h"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/light.h"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/material.h"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/material.inl"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/matrix3x3.h"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/matrix3x3.inl"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/matrix4x4.h"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/matrix4x4.inl"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/mesh.h"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/postprocess.h"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/quaternion.h"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/quaternion.inl"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/scene.h"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/metadata.h"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/texture.h"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/types.h"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/vector2.h"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/vector2.inl"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/vector3.h"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/vector3.inl"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/version.h"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/cimport.h"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/importerdesc.h"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/Importer.hpp"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/DefaultLogger.hpp"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/ProgressHandler.hpp"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/IOStream.hpp"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/IOSystem.hpp"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/Logger.hpp"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/LogStream.hpp"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/NullLogger.hpp"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/cexport.h"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/Exporter.hpp"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/DefaultIOStream.h"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/DefaultIOSystem.h"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/SceneCombiner.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xassimp-devx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/assimp/Compiler" TYPE FILE FILES
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/Compiler/pushpack1.h"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/Compiler/poppack1.h"
    "/home/litleo/Documents/katikGL/pull_request/master/external/Assimp/code/../include/assimp/Compiler/pstdint.h"
    )
endif()

